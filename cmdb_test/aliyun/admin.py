from django.contrib import admin

# Register your models here.
from .models import cn_user as user
from .models import cn_project as project
from .models import cn_access_key as key
from .models import cn_host as host

class admin_user(admin.ModelAdmin):
	list_display = ('u_name', 'u_six', 'u_phone', 'u_email', 'u_remarks',)
	search_fields = ('u_name', 'u_six', 'u_phone', 'u_email',)
	list_filter = ('u_name', 'u_six', 'u_phone', 'u_email',)

class admin_key(admin.ModelAdmin):
	list_display = ('a_name', 'a_platfrom', 'a_key', 'a_remarks',)
	search_fields = ('a_name', 'a_platfrom', 'a_key', 'a_remarks',)

class admin_project(admin.ModelAdmin):
	list_display = ('p_name', 'p_code', 'p_kaifa', 'p_yunwei', 'p_xx', 'p_renew', 'p_remarks',)
	search_fields = ('p_name', 'p_code', 'p_kaifa', 'p_yunwei', 'p_xx', 'p_renew', 'p_remarks',)

class admin_host(admin.ModelAdmin):
	list_display = ('h_name', 'h_project', 'h_instancetype', 'h_region', 'h_publicip', 'h_private', 'h_endtime', 'h_price', )
	search_fields = ('h_name', 'h_project', 'h_instancetype', 'h_region', 'h_publicip', 'h_private', 'h_endtime', 'h_price', )

admin.site.register(user, admin_user)
admin.site.register(key, admin_key)
admin.site.register(project, admin_project)
admin.site.register(host, admin_host)

admin.AdminSite.site_header = 'CMDB_TEST'