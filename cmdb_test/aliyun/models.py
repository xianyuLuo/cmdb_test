from django.db import models

# Create your models here.

#定义联系人的时候选用
SIX_CHOICES = (
	('男', '男'),
	('女', '女'),
)

#定义项目的运营方式时选用
XX_CHOICES = (
	('代理', '代理'),
	('自研', '自研'),
)

#定义账号KEY时选用
PLATFROM_CHOICES = (
	('aliyun', 'aliyun'),
	('qcloud', 'qcloud'),
	('ksyun', 'ksyun'),
	('ucloud', 'ucloud'),
)

#主机模型中“负载”选用
LOAD_CHOICES = (
	('超低负载', '超低负载'),
	('低负载', '低负载'),
	('正常', '正常'),
	('高负载', '高负载'),
)

#联系人定义
class cn_user(models.Model):
	u_name = models.CharField('姓名', max_length = 20, blank = False, unique = True)
	u_six = models.CharField('性别', max_length = 10, choices = SIX_CHOICES, default = '男')
	u_phone = models.CharField('电话', max_length = 11, blank = True)
	u_email = models.EmailField('邮件', max_length = 30, blank = True)
	u_remarks = models.TextField('备注', max_length= 100, blank = True)

	def __str__(self):
		return self.cn_name

	class Meta:
		db_table = 'cn_user'
		verbose_name = '用户'
		verbose_name_plural = verbose_name

#账号KEY定义
class cn_access_key(models.Model):
	a_name = models.CharField('用户名', max_length=10, unique = True, blank = False)
	a_platfrom = models.CharField('云平台', max_length = 10, choices = PLATFROM_CHOICES)
	a_key = models.CharField('KEY', max_length = 200, default = "")
	a_secret = models.CharField('秘钥', max_length = 200, default = "")
	a_remarks = models.TextField('备注', max_length = 200, blank = True)

	def __str__(self):
		return self.a_name

	class Meta:
		db_table = 'cn_access_key'
		verbose_name = '用户KEY'
		verbose_name_plural = verbose_name

#项目定义
class cn_project(models.Model):
	p_name = models.CharField('项目/部门名称', max_length = 50, blank = False)
	p_code = models.CharField('项目代码', max_length = 50, blank = True)
	p_kaifa = models.ForeignKey(cn_user, to_field = 'u_name', related_name = 'ba', verbose_name = '开发', max_length = 10, blank = True)
	p_yunyin = models.ForeignKey(cn_user, to_field = 'u_name', related_name = 'bb', verbose_name = '运营', max_length = 10, blank = True)
	p_yunwei = models.ForeignKey(cn_user, to_field = 'u_name', related_name = 'bc', verbose_name = '运维', max_length = 10, blank = True)
	p_xx = models.CharField('项目运营方式', choices = XX_CHOICES, max_length = 10, default = '代理', blank = True)
	p_renew = models.ForeignKey(cn_user, to_field = 'u_name', related_name = 'bd', verbose_name = '续费确认', max_length = 10, blank = True, default = "")
	p_remarks = models.TextField('备注', max_length = 100, blank = True)

	def __str__(self):
		return "%s(%s)" % (self.p_name, self.p_code)

	class Meta:
		db_table = 'cn_project'
		verbose_name = '项目'
		verbose_name_plural = verbose_name



#主机定义模型
class cn_host(models.Model):
	h_account = models.CharField('所属云账号', max_length = 30, blank = True, default = "")
	h_name = models.CharField('实例名称', max_length = 100, blank = True, default = "")
	h_project = models.ForeignKey(cn_project, verbose_name = '所属项目', max_length = 10, blank = True, default = "")
	h_instancetype = models.CharField('配置', max_length = 50, blank = True, default = "")
	h_region = models.CharField('地域', max_length = 50, blank = True, default = "")
	h_cipan = models.CharField('磁盘', max_length = 50, blank = True, default = "")
	h_publicip = models.CharField('外网IP', max_length = 50, blank = True, default = "")
	h_private = models.CharField('内网IP', max_length = 50, blank = True, default = "")
	h_endtime = models.CharField('到期时间', max_length = 50, blank = True, default = "")
	h_price = models.CharField('价格/月', max_length = 10, blank = True, default = "")
	h_load = models.CharField('负载', max_length = 10, blank = True, default = "NULL")
	h_createtime = models.CharField('创建时间', max_length = 20, blank = True, default = "")
	h_pay = models.CharField('付费方式', max_length = 10, blank = True, default = "")
	h_meta = models.TextField('主机元数据', blank = True, default = "")
	h_remarks = models.TextField('备注', blank = True, default = "")

	def __str__(self):
		return self.h_name

	class Meta:
		db_table = 'cn_host'
		verbose_name = '主机'
		verbose_name_plural = verbose_name